Step 1 — Set Up the Workspace Directory and Ansible Inventory File

In this section, you will create a directory on your local machine that will serve as your workspace. You will configure Ansible locally so that it can communicate with and execute commands on your remote servers. Once that’s done, you will create a hosts file containing inventory information such as the IP addresses of your servers and the groups that each server belongs to.

Out of your three servers, one will be the control plane with an IP displayed as master_node_ip. The other two servers will be workers and will have the IPs worker1_node_ip and worker2_node_ip.

Create a directory named ~/kube-cluster (you can name it whatever you want) in the home directory of your local machine and cd into it:

$ mkdir ~/kube-cluster
$ cd ~/kube-cluster

Create a file named ~/kube-cluster/hosts using nano or your favorite text editor:

$ nano ~/kube-cluster/hosts

[masters]
master1 ansible_host=master_node_ip ansible_user=root 

[workers]
worker1 ansible_host=worker1_node_ip ansible_user=root
worker2 ansible_host=worker2_node_ip ansible_user=root

[all:vars]
ansible_python_interpreter=/usr/bin/python3

All this yaml files you can clone from the repo!!!


Step 2 — Creat a Non-Root User on All Remote Servers

Create a file named ~/kube-cluster/initial.yml in the workspace:

$ nano ~/kube-cluster/initial.yml

---
- hosts: all
  become: yes
  tasks:
    - name: create the 'ubuntu' user
      user: name=ubuntu append=yes state=present createhome=yes shell=/bin/bash

    - name: allow 'ubuntu' to have passwordless sudo
      lineinfile:
        dest: /etc/sudoers
        line: 'ubuntu ALL=(ALL) NOPASSWD: ALL'
        validate: 'visudo -cf %s'

    - name: set up authorized keys for the ubuntu user
      authorized_key: user=ubuntu key="{{item}}"
      with_file:
        - ~/.ssh/id_rsa.pub

Save and run the playbook locally:

$ ansible-playbook -i hosts ~/kube-cluster/initial.yml

Step 3 — Installing Kubernetetes’ Dependencies

In this section, you will install the operating-system-level packages required by Kubernetes with Ubuntu’s package manager. These packages are:
Docker - a container runtime. It is the component that runs your containers. Kubernetes supports other runtimes, but Docker is still a popular and straightforward choice.
kubeadm - a CLI tool that will install and configure the various components of a cluster in a standard way.
kubelet - a system service/program that runs on all nodes and handles node-level operations.
kubectl - a CLI tool used for issuing commands to the cluster through its API Server.

Create a file named ~/kube-cluster/kube-dependencies.yml in the workspace:

$ nano ~/kube-cluster/kube-dependencies.yml

Next, run the playbook locally with the following command:

$ ansible-playbook -i hosts ~/kube-cluster/kube-dependencies.yml

Step 4 — Setting Up the Control Plane Node 

In this section, you will set up the control plane node. Before creating any playbooks, however, it’s worth covering a few concepts such as Pods and Pod Network Plugins, since your cluster will include both.

A pod is an atomic unit that runs one or more containers. These containers share resources such as file volumes and network interfaces in common. Pods are the basic unit of scheduling in Kubernetes: all containers in a pod are guaranteed to run on the same node that the pod is scheduled on.

Each pod has its own IP address, and a pod on one node should be able to access a pod on another node using the pod’s IP. Containers on a single node can communicate easily through a local interface. Communication between pods is more complicated, however, and requires a separate networking component that can transparently route traffic from a pod on one node to a pod on another.

This functionality is provided by pod network plugins. For this cluster, you will use Flannel, a stable and performant option.

Create an Ansible playbook named control-plane.yml on your local machine:

$ nano ~/kube-cluster/masters.yml


Here’s a breakdown of this play:

The first task initializes the cluster by running kubeadm init. Passing the argument --pod-network-cidr=10.244.0.0/16 specifies the private subnet that the pod IPs will be assigned from. Flannel uses the above subnet by default; we’re telling kubeadm to use the same subnet.

The second task creates a .kube directory at /home/ubuntu. This directory will hold configuration information such as the admin key files, which are required to connect to the cluster, and the cluster’s API address.

The third task copies the /etc/kubernetes/admin.conf file that was generated from kubeadm init to your non-root user’s home directory. This will allow you to use kubectl to access the newly-created cluster.

The last task runs kubectl apply to install Flannel. kubectl apply -f descriptor.[yml|json] is the syntax for telling kubectl to create the objects described in the descriptor.[yml|json] file. The kube-flannel.yml file contains the descriptions of objects required for setting up Flannel in the cluster.

Save and close the file when you are finished.

Run the playbook locally with the following command:

$ ansible-playbook -i hosts ~/kube-cluster/masters.yml


To check the status of the control plane node, SSH into it with the following command:

$ ssh ubuntu@master_node_ip

$ kubectl get nodes



Step 5 — Setting Up the Worker Nodes

Adding workers to the cluster involves executing a single command on each. This command includes the necessary cluster information, such as the IP address and port of the control plane’s API Server, and a secure token. Only nodes that pass in the secure token will be able join the cluster.

Navigate back to your workspace and create a playbook named workers.yml:

$ nano ~/kube-cluster/workers.yml

Here’s what the playbook does:

The first play gets the join command that needs to be run on the worker nodes. This command will be in the following format:kubeadm join --token <token> <master-node-ip>:<master-node-port> --discovery-token-ca-cert-hash sha256:<hash>. Once it gets the actual command with the proper token and hash values, the task sets it as a fact so that the next play will be able to access that info.

The second play has a single task that runs the join command on all worker nodes. On completion of this task, the two worker nodes will be part of the cluster.

Save and close the file when you are finished.

Run the playbook by locally with the following command:

$ ansible-playbook -i hosts ~/kube-cluster/workers.yml


Step 6 — Verifying the Cluster

$ ssh ubuntu@master_node_ip

$ kubectl get nodes


All configuration stored in the yml files!!!